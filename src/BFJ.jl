module BFJ

using HDF5
using Plots
using Base
import JSON

export
    @unpack,
    load_params,
    load_file,
    load_package,
    package_label,
    package_meta,
    packages,
    instance_id,
    param_name,
    load_packages,
    load_inputs,
    save_file,
    save_package

global
    package_index

package_index = 0

#---------------------------------------------------------------------------
function load_hdf5(fname)
    v = Dict{String, Any}()
    f = h5open(fname)
    try
        for n in names(f)
            if n[1] != '_'
                v[n] = read(f, n)
            end
        end
    finally
        close(f)
    end

    return v
end

#---------------------------------------------------------------------------
function load_file(fname)
    return load_hdf5(fname)
end

#---------------------------------------------------------------------------
function package_meta(path)
    v = Dict{String, Any}()

    meta = joinpath(path, "meta")
    if isdir(meta)
        files = [joinpath(meta, f) for f in readdir(meta)]
        for f in files
            data = strip(read(f, String))
            try
                data = JSON.parse(data)
            catch e
                println("JSON parse error: ", f, data)
            end
            v[splitdir(f)[2]] = data
        end
    end

    return v
end

#---------------------------------------------------------------------------
function download_file(path, f)
    fname = joinpath(path, f["name"])
    download(f["link"], fname)
    if f["executable"]
        chmod(fname, 0o770)
    end
end

#---------------------------------------------------------------------------
function load_package(path, meta=true, clean_up=true)
    links = joinpath(path, ".files")
    if isfile(links)
        files = JSON.parse(strip(read(links, String)))
        for f in files
            download_file(path, f)
        end
    else
        clean_up = false
    end

    files = [joinpath(path, f) for f in readdir(path)]
    v = Dict{String, Any}()

    for f in files
        fname = splitdir(f)[2]

        if ~isfile(f) || fname == "label"
            continue
        end

        try
            if endswith(fname, ".h5") || endswith(fname, ".hdf5")
                nam = splitext(fname)[1]
                val = load_file(f)

                if length(val) == 1
                    v[nam] = first(values(val))
                else
                    v[nam] = val
                end

                if clean_up
                    rm(f)
                end
            end
        catch e
            println("Skipped file ", f)
            showerror(e, backtrace())
        end
    end

    if meta
        merge!(v, package_meta(path))
    end

    return v
end

#---------------------------------------------------------------------------
function package_label(path)
    label = joinpath(path, "label")
    if isfile(label)
        return strip(read(label, String))
    end

    return splitdir(path)[2]
end

#---------------------------------------------------------------------------
function packages()
    zip = joinpath("in", ".data.zip")
    if isfile(zip)
        run(`unzip -o $zip -d in`)
    end
    p = Array{Tuple{String, String}, 1}()
    files = [joinpath("in", f) for f in readdir("in")]
    for f in files
        if isdir(f)
            push!(p, (f, package_label(f)))
        end
    end
    return p
end

#---------------------------------------------------------------------------
function instance_id()
    return parse(Int64, splitdir(realpath("."))[2])
end

#---------------------------------------------------------------------------
function param_name()
    for f in readdir(".")
        if isfile(f) && endswith(f, ".prm")
            return f
        end
    end
    return ".prm"
end

#---------------------------------------------------------------------------
function load_packages(meta=true)
    v = Dict{String, Any}()
    for (p,l) in packages()
        merge!(v, load_package(p, meta)) 
    end
    return v
end

#---------------------------------------------------------------------------
function load_params()
    if isfile(param_name())
        return Dict(
                    p["key"] => JSON.parse(p["val"])
                    for p in JSON.parse(read(param_name(), String)))
    end
end

#---------------------------------------------------------------------------
function load_inputs()
    v = Dict{String, Any}()
    merge!(v, load_packages())
    merge!(v, load_params())
    return v
end

#---------------------------------------------------------------------------
function unpack(m, d)
    for (k,v) in d
        Core.eval(m, :($(Symbol(k)) = $v))
    end
end

#---------------------------------------------------------------------------
if VERSION >= v"0.7.0"
    macro cur_module()
        quote
            $__module__
        end
    end
else
    macro cur_module()
        quote
            current_module()
        end
    end
end

#---------------------------------------------------------------------------
macro unpack(d)
    quote
        unpack(@cur_module, $(esc(d)))
    end
end

#---------------------------------------------------------------------------
function save_variable(f, path, val)
    if isa(val, Array) && isa(val[1], Array)
        d_write(f, path, HDF5.HDF5Vlen(val), "track_times", false)
    else
        d_write(f, path, val, "track_times", false)
    end
end

#---------------------------------------------------------------------------
function save_variable(f, path, val::Dict)
    for k = keys(val)
        save_variable(f, joinpath(path, string(k)), val[k])
    end
end

#---------------------------------------------------------------------------
function save_file(fname, vars)
    f = h5open(fname, "w")
    try
        for (k,v) in vars
            save_variable(f, k, v)
        end
    finally
        close(f)
    end
end

#---------------------------------------------------------------------------
function save_package(; name="", label="",
                      data=Dict{String, Any}(),
                      meta=Dict{String, String}(),
                      image=Dict{String, Any}()
                     )
    if isempty(name)
        global package_index += 1
        path = joinpath("out", string(package_index))
    else
        path = joinpath("out", name)
        while isdir(path)
            global package_index += 1
            path = joinpath("out", string(name, "_", package_index))
        end
    end

    mkpath(path)

    if ~isempty(label)
        open(joinpath(path, "label"), "w") do f
            write(f, label)
        end
    end

    for (k,v) in data
        save_file(joinpath(path, string(k, ".h5")), Dict(k=>v))
    end

    if ~isempty(meta)
        mkpath(joinpath(path, "meta"))
        for (k,v) in meta
            open(joinpath(path, "meta", k), "w") do f
                try
                    write(f, JSON.json(v))
                catch e
                    write(f, string(v))
                end
            end
        end
    end

    if ~isempty(image)
        mkpath(joinpath(path, "image"))
        for (k,v) in image
            png(v, joinpath(path, "image", string(k, ".png")))
        end
    end
end

end
